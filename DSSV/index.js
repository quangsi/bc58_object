var dssv = [];

var dataJson = localStorage.getItem("DSSV_LOCAL");
if (dataJson != null) {
  let result = JSON.parse(dataJson);
  console.log("😀 - result", result);
  // convert 1 array chứa object ko có method trở thành 1 array chứa object có method
  // map() ~ convert ~ duyệt mảng vào trả về mảng mới
  dssv = result.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.toan,
      item.van
    );
  });
  console.log("😀 - dssv", dssv);
  renderDssv(dssv);
}

function themSv() {
  //
  /**
   * 1. tạo 1 array rỗng
   * 2. lấy thông tin từ form => tạo object => push object vào array
   * 3. render array object lên layout ( tạo các thẻ tr tương ứng )
   */
  var _ma = document.getElementById("txtMaSV").value;
  var _ten = document.getElementById("txtTenSV").value;
  var _email = document.getElementById("txtEmail").value;
  var _matKhau = document.getElementById("txtPass").value;
  var _toan = document.getElementById("txtDiemToan").value * 1;
  var _van = document.getElementById("txtDiemVan").value * 1;
  var sv = new SinhVien(_ma, _ten, _email, _matKhau, _toan, _van);

  // validate
  // maSv
  var isValid =
    kiemTraMaSv(sv.ma, dssv) & kiemTraDoDai(sv.ma, "spanMaSV", 4, 6);
  // matKhau
  isValid = isValid & kiemTraDoDai(sv.matKhau, "spanMatKhau", 7, 8);
  // email
  isValid = isValid & kiemEmail(sv.email);
  if (isValid) {
    dssv.push(sv);
    // lưu thông tin vào localStore
    var dataJson = JSON.stringify(dssv);
    localStorage.setItem("DSSV_LOCAL", dataJson);
    renderDssv(dssv);
  }
}

function xoaSv(id) {
  /**
   * splice(vị trí cần xoá, số lượng xoá)
   * 1.từ id tìm vị trí => findIndex
   * 2. sử dụng splice để remove
   * 3. update lại layout
   */
  console.log("😀 - xoaSv - id", id);
  var viTri = dssv.findIndex(function (item) {
    // item : từng phần tử trong array dssv
    return item.ma == id;
  });
  dssv.splice(viTri, 1);
  renderDssv(dssv);
}
function suaSv(id) {
  // tìm vị trí của item đang có trong dssv với điều kiện item.ma == id
  var viTri = dssv.findIndex(function (item) {
    return item.ma == id;
  });
  var sv = dssv[viTri];
  console.log("😀 - suaSv - sv", sv);
  // ko cho user update id
  document.getElementById("txtMaSV").readOnly = true;
  // đưa thong tin len form
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtPass").value = sv.matKhau;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemVan").value = sv.van;
}
function capNhat() {
  /**
   * 1. tìm vị trí dựa vào id => viTri
   * 2. lay thong tin tu form => sv
   * 3.  dssv[viTri]=sv
   */
}
/**
 * JSON:  JSON.stringtify() ~  array => json , JSON.parse() json=> array : convert data gốc về định dạng json
 * localStorage : lưu trữ, chỉ lưu được định dạng json
 */
