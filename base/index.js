// array : lưu danh sách ( danh sách điểm ), truy xuất => array[index]

// object :mô tả nhiều tin về 1 đối tượng, truy xuất => object.key

// 1 bạn gồm 5 thông tin => cần 5 biến
// 10 bạn => 50 biến
// dùng object => 10 biến

// key : value ,

// property ~ key chứa data , method ~ key chứa function

var cat1 = {
  name: "miu miu",
  age: 2,
};
var cat2 = {
  name: "meo meo",
  age: 4,
  sayHello() {
    console.log("meo meo meo , tao là : ", this.name);
  },
};
console.log("😀 - cat2", cat2.sayHello());
console.log("😀 - name", cat1.name);

/**
 data trong js chia thành 2 nhóm:pass by value , pass by reference
 + pass by value: string,nuber,boolean,...
 + pass by reference: array, object
 */

var dog1 = {
  name: "lulu",
  score: 5,
};
var dog2 = dog1;
dog2.score = 10;

var a = 1;
var b = a;
b = 2;
console.log("😀 - a", a);
// lớp đối tượng Cat
function Cat(id, name) {
  this.ma = id;
  this.ten = name;
}
// tạo cat3 từ class Cat

var cat3 = new Cat(1, "mi mi");
var cat4 = new Cat(2, "me me");
console.log("😀 - cat3", cat3);
console.log("😀 - cat4", cat4);
